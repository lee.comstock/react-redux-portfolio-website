import { configureStore, createSlice } from '@reduxjs/toolkit'

const slice = createSlice({
  name: 'slice',
  initialState: {
    content: undefined as {} | undefined,
    transition: false as boolean,
    page: "Splash Page" as string,
    route: ["Splash Page"] as string[],
    fullscreenImageDisplay: undefined as string | undefined,
  },
  reducers: {
    setContent(state, action) {
      state.content = action.payload;
    },
    setTransition(state, action) {
      state.transition = action.payload;
    },
    setFullscreenImageDisplay(state, action) {
      state.fullscreenImageDisplay = action.payload;
    },
    setPageToSplashPage(state) {
      state.route = ["Splash Page"];
    },
    setPageFromClick(state, action) {
      if (action.payload) {
        // switch to page
        state.page = action.payload;
        if (state.route.includes(action.payload)) {
          // going back to a previous page, adjust route array
          state.route.length = state.route.indexOf(action.payload) + 1;
        } else {
          // going to a new page, add to route array
          state.route.push(action.payload);
        }
      } else {
        // if no new page string is passed in, go back one step
        state.route.pop();
        state.page = state.route[state.route.length - 1];
      }
      // push new state into url history
      const urlRouteArray = [...state.route];
      urlRouteArray.shift();
      const urlRouteString = "/" + urlRouteArray.join('_').split(' ').join('-');
      window.history.pushState({}, "", urlRouteString);
    },
    setPageFromUrl(state) {
      const pagesFromUrl = window.location.pathname.split('/').join('')?.length
      ? window.location.pathname.split('/').join('').split('-').join(' ').split('_')
      : [];
      state.route = ["Splash Page", ...pagesFromUrl];
      state.page = state.route[state.route.length - 1];
    }
  },
})

export async function getContent(dispatch: Function) {
  dispatch(actions.setPageFromUrl());
  const response = await fetch('./data/content.json');
  const json = await response.json();
  dispatch(actions.setContent(json));
}

// change page, if no new page string is passed in, it will go back one step
export function changePage(dispatch: Function, newPage?: string, setPageFromUrl?: boolean) {
  dispatch(actions.setTransition(true));
  setTimeout(() => {
    dispatch(setPageFromUrl
      ? actions.setPageFromUrl()
      : actions.setPageFromClick(newPage)
    );
  }, 1000);
  setTimeout(() => dispatch(actions.setTransition(false)), 1100);
}

const store = configureStore({
  reducer: {
    reducer: slice.reducer,
  },
})

export const actions = slice.actions

export default store;
