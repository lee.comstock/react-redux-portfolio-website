import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import { changePage, actions } from '../store/store';
import './MobileMenu.scss';

export default function MobileMenu() {
  const page = useSelector(state => (state as any).reducer.page)
  const route = useSelector(state => (state as any).reducer.route);
  const links = useSelector(state => {
    return Object.keys((state as any).reducer.content || {})
      .filter(key => (state as any).reducer.content[key].type !== "navigation")
      .map(key => ({
        text: key,
        color: (state as any).reducer.content[key].color,
      }));
  });
  const dispatch = useDispatch();
  const [expanded, setExpanded] = useState(false);

  return (
    <div className={`mobile-menu
      ${ route.length < 2 ? "hidden"   : "" }
      ${ expanded         ? "expanded" : "" }
    `}>
      <button type="button" className="mobile-menu-back-button"
      onClick={() => {
        setExpanded(false);
        changePage(dispatch);
      }}>
      Back
      </button>
      <button type="button" className="mobile-menu-expand-button"
      onClick={() => setExpanded(!expanded)}>
        Expand
        <div></div>
        <div></div>
        <div></div>
      </button>
      <div className="mobile-menu-list">
        <nav>
          { links.map(link => {
            return (
              <a key={link.text} href="/"
              className={ page === link.text ? "selected" : "" }
              style={{ background: page === link.text ? link.color : "none" }}
              onClick={(event) => {
                event.preventDefault();
                if (page === link.text) return;
                setExpanded(false);
                dispatch(actions.setPageToSplashPage());
                changePage(dispatch, link.text);
              }}>
                <div className="mobile-menu-list-bullet"
                style={{ background: page === link.text ? "white" : link.color }}>
                </div>
                {link.text}
              </a>
            )
          })}
        </nav>
      </div>
    </div>
  )
}
