import { useSelector, useDispatch } from 'react-redux'
import { changePage } from '../store/store'
import './Breadcrumb.scss';

export default function Breadcrumb() {
  const route = useSelector(state => (state as any).reducer.route)
  const dispatch = useDispatch()

  return (
    <div className={`breadcrumb-container ${ route.length < 2 ? "hidden" : "" }`}>
      <div className="breadcrumb-list">
        { route.map((page: string, index: number) => {
          return (
            <button type="button" key={index}
            style={{ zIndex: 10 - index }}
            onClick={() => {
              if (index !== route.length - 1) changePage(dispatch, page);
            }}>
              {page}
            </button>
          )
        })}
      </div>
      <div className="breadcrumb-back-button-container">
        <button type="button" className="breadcrumb-back-button"
        onClick={() => changePage(dispatch)}>
        Back
        </button>
      </div>
    </div>
  )
}
