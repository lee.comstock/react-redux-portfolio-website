import { useDispatch } from 'react-redux'
import { changePage } from '../store/store'
import './Box.scss';

type boxProps = {
  id: string,
  title: string,
  color: string,
  image: string,
}

export default function Box({ id, title, color, image } : boxProps) {
  const dispatch = useDispatch()

  return (
    <div className={"box " + id}
      onMouseEnter={() => boxHover(id)}
      onClick={() => changePage(dispatch, title)}>
      <div className="box-inner">
        <div className="box-bg-image" style={{ backgroundImage: `url(images/${image})` }}></div>
        <div className="box-color"    style={{ backgroundColor: color }}></div>
        <div className="box-cover"></div>
        <h2>{title}</h2>
      </div>
    </div>
  )
}

function boxHover(id: string) {
  if (window.innerWidth > 1000) {
    let boxContainer = document.getElementsByClassName('content')[0];
    let boxHovered   = document.getElementsByClassName(id)[0];
    boxContainer.insertBefore(boxHovered, (boxContainer.lastChild as ChildNode).nextSibling);
  }
}
