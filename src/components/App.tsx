import { useSelector, useDispatch } from 'react-redux'
import { useState, useEffect } from 'react';
import { getContent, changePage } from '../store/store'
import './App.scss';
import Box from './Box';
import PageBox from './PageBox';
import Breadcrumb from './Breadcrumb';
import MobileMenu from './MobileMenu';
import SplashPageBox from './SplashPageBox';
import FullscreenImageDisplay from './FullscreenImageDisplay';

export default function App() {
  const page = useSelector(state => (state as any).reducer.page)
  const content = useSelector(state => (state as any).reducer.content)
  const transition = useSelector(state => (state as any).reducer.transition)
  const fullscreenImageDisplay = useSelector(state => (state as any).reducer.fullscreenImageDisplay)

  const [intro, setIntro] = useState(true);
  setTimeout(() => setIntro(false), 1000);

  const dispatch = useDispatch();
  // eslint-disable-next-line
  useEffect(() => { getContent(dispatch) }, []);

  window.onpopstate = () => { changePage(dispatch, undefined, true) };

  return (
    <div className={`app ${transition ? "transition" : ""} ${intro ? "intro" : ""}`}>
      <Breadcrumb/>
      { content ? (() => {
        switch(content[page].type) {
          case "root": return (
            <div className="content">
              <SplashPageBox
              name={content[page].name}
              title={content[page].title}
              image={content[page].profileImage}
              />
            </div>
          )
          case "navigation": return (
            <div className={`content box-sum-${content[page].options.length}`}>
              { content[page].options.map((option: string, index: number) => {
                return (
                  <Box
                  id={ "box-" + (index + 1)}
                  key={"box-" + (index + 1)}
                  title={option}
                  color={content[option]?.color}
                  image={content[option]?.backgroundImage}
                  />
                )
              })}
            </div>
          )
          case "content": return (
            <div className="content">
              <PageBox
              key={page}
              title={page}
              color={content[page].color}
              image={content[page].backgroundImage}
              links={content[page].links}
              images={content[page].images}
              description={content[page].description}
              />
            </div>
          )
        }
      })() : undefined }
      <MobileMenu/>
      { fullscreenImageDisplay ? <FullscreenImageDisplay/> : undefined }
    </div>
  );
}
