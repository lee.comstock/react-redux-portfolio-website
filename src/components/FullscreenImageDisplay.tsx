import { useDispatch, useSelector } from 'react-redux'
import { useState } from 'react';
import { actions } from '../store/store'
import './FullscreenImageDisplay.scss';

export default function FullscreenImageDisplay() {
  const image = useSelector(state => (state as any).reducer.fullscreenImageDisplay);
  const dispatch = useDispatch();
  const [transitionOut, setTransitionOut] = useState(false);

  return (
    <div className={`fullscreen-image-display ${transitionOut ? "transition-out" : ""}`}>
      <div className="fullscreen-image-display-image" style={{ backgroundImage: `url(images/${image})` }}>
      </div>
      <button className="fullscreen-image-display-close-button"
      type="button"
      onClick={() => {
        setTransitionOut(true);
        setTimeout(() => dispatch(actions.setFullscreenImageDisplay(undefined)), 500);
      }}>
      </button>
    </div>
  )
}
