import { useDispatch } from 'react-redux'
import { changePage } from '../store/store'
import './SplashPageBox.scss';

type splashPageBoxProps = {
  name: string,
  title: string,
  image: string,
}

export default function SplashPageBox({ name, title, image } : splashPageBoxProps) {
  const dispatch = useDispatch()
  
  return (
    <div className="splash-page-box page-box">
      <div className="box-bg-image" style={{ backgroundImage: "url(images/Mech.png)" }}></div>
      <div className="box-color"></div>
      <div className="box-cover"></div>
      <div className="splash-page-box-profile-pic" style={{
        backgroundImage: `url(images/${image})`,
      }}>
      </div>
      <div className="splash-page-box-text-container">
        <h1 className="splash-page-box-name">{name}</h1>
        <h2 className="splash-page-box-title">{title}</h2>
      </div>
      <div className="splash-page-button-container">
        <button type="button" onClick={() => changePage(dispatch, "Personal")}>Personal</button>
        <button type="button" onClick={() => changePage(dispatch, "Business")}>Business</button>
      </div>
    </div>
  )
}
