import { useDispatch } from 'react-redux'
import { useState } from 'react';
import { actions } from '../store/store'
import './PageBox.scss';

type pageBoxProps = {
  title: string,
  description: string,
  color: string,
  image: string,
  links: link[],
  images: string[],
}

type link = {
  url: string,
  text: string,
}

export default function PageBox({
  title,
  description,
  color,
  image,
  links,
  images,
} : pageBoxProps) {
  const dispatch = useDispatch();
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  return (
    <div className="page-box">
      <div className="box-bg-image" style={{ backgroundImage: `url(images/${image})` }}></div>
      <div className="box-color"    style={{ backgroundColor: color }}></div>
      <div className="box-cover"></div>
      <div className="page-box-left-content">
        <h1>{title}</h1>
        <p dangerouslySetInnerHTML={{__html: description}}/>
        { links.map((link, index) => {
          return (
            <a href={link.url} key={index} target="_blank" rel="noreferrer">{link.text}</a>
          )
        })}
      </div>
      <div className="page-box-right-content">
        <div className="page-box-image-container">
          <div className="page-box-image-offset" style={{ marginLeft: (currentImageIndex * -100) + "%" }}></div>
          { images.map((img, index) => {
            return (
              <div className="page-box-image"
              key={index}
              onClick={() => dispatch(actions.setFullscreenImageDisplay(img))}
              style={{ backgroundImage: `url(images/${img})` }}>
                <div className="page-box-image-overlay">
                  <div className="page-box-image-overlay-icon">
                    <div className="page-box-image-overlay-icon-corner"></div>
                    <div className="page-box-image-overlay-icon-corner"></div>
                    <div className="page-box-image-overlay-icon-corner"></div>
                    <div className="page-box-image-overlay-icon-corner"></div>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
        { images.length > 1 ?
          <div className="page-box-image-buttons">
            <input type="button" value="previous"
            disabled={!currentImageIndex}
            onClick={() => setCurrentImageIndex(currentImageIndex - 1)}/>
            <input type="button" value="next"
            disabled={currentImageIndex === images.length - 1}
            onClick={() => setCurrentImageIndex(currentImageIndex + 1)}/>
          </div>
        : undefined }
      </div>
    </div>
  )
}
